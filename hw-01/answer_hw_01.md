## 1. Indica la diferencia entre el uso de la instrucción CMD y ENTRYPOINT (Dockerfile)

La instrucción `ENTRYPOINT` nos permite configurar un contenedor que se comportará como un ejecutable en el momento de lanzar el
contenedor; concretamente, nos permite especificar cuál es el "punto de entrada" que disparará el contenedor.

Por otra parte, la instrucción `CMD` tiene como principal objetivo proveer al contenedor de aquellos comandos que se desean ejecutar en el
momento de lanzarlo y que, generalmente, lanzan procesos que se quedan activos o escuchando, como un servidor web.

Centrándonos en la pregunta del enunciado, pues, podemos enumerar algunas de las principales diferencias entre ambas instrucciones:

1. `ENTRYPOINT`
    1. Su uso está orientado a aquellos contendores que se comportarán como si fueran un ejecutable, por ejemplo, podríamos pasar como
       valor `["node"]`, lo que haría que al inicial el contenedor, este esté ejecutando el proceso `node`.
    2. Un Dockerfile puede tener varias instrucciones `ENTRYPOINT`; sin embargo, solamente la última especificada tendrá efecto

2. `CMD`
    1. Es fácilmente sobreescribible, y puede ser usado para proveer de argumentos por defecto para la propia instrucción `ENTRYPOINT`
    2. Se suele usar para ejecutar un comando 'ad-hoc' en el contendor en el momento en que este sea lanzado. Por ejemplo, si tenemos la
       lógica de ejecución de un servidor web dentro de nuestro `app.js`, entonces en vez de lanzar `node` a secas especificaríamos `node
       app.js`
    3. Solamente puede haber una única instrucción `CMD` en un Dockerfile

## 2. Indica la diferencia entre el uso de la instrucción ADD y COPY

Ambas instrucciones nos sirven para añadir ficheros a los contenedores, sin embargo presentan algunas diferencias.

La instrucción `ADD` se utiliza para clonar nuevos directorios o archivos de un origen y añadirlos al sistema de archivos de una
determinada IMAGEN de destino. Tiene, además, una particularidad que no encontramos en `COPY`: acepta como "fuente de origen" de
archivos o carpetas a copiar una "url" de la que descargar la fuente a copiar. Así mismo, una propiedad única de `ADD` es que nos
permite copiar, incluso, archivos desde un archivo comprimido tipo `.tar`.

Por otra parte, la instrucción `COPY` copia, valga la redundancia, carpetas o archivos y los añade al sistema de archivos de un
CONTENEDOR; nos permite copiar archivos en contexto de build al contenedor. `COPY` solamente nos deja copiar desde nuestro host.

## 3. Crea un contenedor con las siguientes especificaciones

#### Punto 1
Para la realización del primer punto lo primero que hice fue crear un archivo `Dockerfile` con una única instrucción:

```dockerfile
FROM nginx:1.19.3-alpine
```

Usamos la versión alpine al ser más ligera que la principal:

![](./img/02.png)
![](./img/01.png)

A continuación, lanzamos el comando para generar la imagen:

```bash
docker build -t ez-nginx .
```

Y lanzamos el contendor con el comando:

```bash
docker run --name homework1 -p 8080:80 ez-nginx
```

Podemos ver cómo el contenedor es lanzado, y accediendo a `http://localhost:8080` obtenemos una respuesta:

![](./img/03.png)

#### Punto 2
Para el punto 2, he tenido en cuenta que la imagen oficial de `nginx` tiene un path específico en el que guarda el HTML base. El
objetivo que me marqué es crear un `index.html` con el contenido sugerido dentro de la ruta, para así que cualquier llamada a
`http://localhost:8080` devuelva este nuevo contenido:

```dockerfile
FROM nginx:1.19.3-alpine
RUN echo "HOMEWORK 1" > /usr/share/nginx/html/index.html
```

Volvemos a lanzar los comandos anteriores:

```bash
docker build -t ez-nginx .
docker run --name homework1 -p 8080:80 ez-nginx 
```

Y vemos el resultado:

![](./img/04.png)
![](./img/05.png)

### Punto 3
Para el punto 3 he decidido usar comandos. Primero he creado una carpeta `static_content` y he añadido un `index.html` dentro, con el
mismo contenido del punto anterior.

![](./img/06.png)

Después de crear la imagen a partir del Dockerfile:

```dockerfile
FROM nginx:1.19.3-alpine
```
```shell
docker build -t ez-nginx .
```

Esta vez lanzamos el comando `run` con algunas diferencias:

```shell
docker run -p 8080:80 --name homework1 -v ${PWD}/static_content:/usr/share/nginx/html ez-nginx
```

Ahora, cada vez que modificamos `index.html` en nuestro local, podremos ver como el contenedor, al momento, actualiza su contenido.

### Punto 4
Para la realización de este punto he añadido al Dockerfile una nueva instrucción `HEALTHCHECK` con los parámetros requeridos:

`--interval`: Intervalo de tiempo entre un check y el siguiente

`--timeout`: Intervalo de tiempo que el healtcheck se esperará para emitir una respuesta con el status correspondiente

`--start-period`: Intervalo de tiempo que el contendor necesita para inicializarse; una especie de "delay" de la propia ejecución del
healsthcheck

`--retries`: Número de fallos requeridos para definir al contendor con un status de `unhealthy`

```dockerfile
FROM nginx:1.19.3-alpine

RUN echo "HOMEWORK 1" > /usr/share/nginx/html/index.html

HEALTHCHECK --interval=45s --timeout=5s --start-period=2s --retries=2 CMD curl --fail http://localhost:80 || exit 1
```

Como vemos, ejecutamos una petición http via `curl` a la ruta donde nuestro contenedor está escuchando: en caso de error, indicamos un
exit 1, como indica la documentación:

`1: unhealthy - the container is not working correctly`

Aquí vemos como la petición con curl es lanzada automáticamente:

![](./img/07.png)

### Punto 5

`docker-compose.yml`:

```yaml
version: '3.6'
services:
  elasticsearch:
    # Utilizar la imagen de elasticsearch v7.9.3
    image: elasticsearch:7.9.3

    # Asignar un nombre al contenedor
    container_name: my-elastic

    # Define las siguientes variables de entorno:
    environment:
      discovery.type: single-node

    # Emplazar el contenedor a la red de elastic
    networks:
      - elk

    # Mapea el Puerto externo 9200 al puerto interno del contenedor 9200
    # Idem para el puerto 9300
    ports:
      - "9200:9200"
      - "9300:9300"

  kibana:
    # Utilizar la imagen kibana v7.9.3
    image: kibana:7.9.3

    # Asignar un nombre al contenedor
    container_name: my-kibana

    # Emplazar el contenedor a la red de elastic
    networks:
      - elk

    # Define las siguientes variables de entorno:
    environment:
      ELASTICSEARCH_HOST: elasticsearch
      ELASTICSEARCH_PORT: 9200

    # Mapear el puerto externo 5601 al puerto interno 5601
    ports:
      - "5601:5601"

    # El contenedor Kibana debe esperar a la disponibilidad del servicio elasticsearch
    depends_on:
      - elasticsearch

# Definir la red elastic (bridge)
networks:
  elk:
    driver: bridge
```

- Resultado:
  ![](./img/11.png)

