# Exercise 1

```yaml
apiVersion: v1
kind: Pod
metadata:
  app: nginx-server
spec:
  containers:
  - name: nginx
    image: nginx:1.19.4
    ports:
    - containerPort: 80
```

# Exercise 2

````yaml
apiVersion: v1
kind: ReplicaSet
metadata:
  name: nginx
  labels:
    app: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
        - name: nginx
          image: nginx:1.19.4
          ports:
            - containerPort: 80
          resources:
            requests:
              memory: "256Mi"
              cpu: "100m"
            limits:
              memory: "256Mi"
              cpu: "100m"
````